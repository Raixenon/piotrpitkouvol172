﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    private Vector3 offset;
    private float zPos;

    void OnMouseDown() {
        zPos = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        offset = gameObject.transform.position - GetMousePos();
    }

    private Vector3 GetMousePos() {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = zPos;

        return Camera.main.ScreenToWorldPoint(mousePos);
    }

	void OnMouseDrag() {
        transform.position = GetMousePos() + offset;
    }
}
