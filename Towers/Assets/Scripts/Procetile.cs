﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Procetile : MonoBehaviour, IPoolObj {

	[SerializeField] float speed = 4f;
	ObjectPooler towerPooler;
	float range = 1f;
	float aliveTime;
	bool stopSpawning = false;
	Rigidbody2D rb;

	void Start() {
		OnObjectSpawn();
	}

	public void OnObjectSpawn() {
		towerPooler = ObjectPooler.Instance;
		rb = GetComponent<Rigidbody2D>();
		rb.velocity = transform.right * speed;
		range = Random.Range(1, 4);
		aliveTime = range / speed;
	}

	void FixedUpdate() {
        aliveTime -= Time.deltaTime;
        SpawnTower();
    }

    private void SpawnTower() {
        if (aliveTime <= Mathf.Epsilon && towerPooler.activeTowers < 100 && !stopSpawning) {
            gameObject.SetActive(false);
			towerPooler.SpawnFromPool("Tower", transform.position, Quaternion.identity);
        }
    }

    void OnCollisionEnter2D(Collision2D coll) {
		if(coll.gameObject.tag == "Tower") {
			coll.gameObject.SetActive(false);
			gameObject.SetActive(false);
			towerPooler.activeTowers -= 1;
		}
    }
}
