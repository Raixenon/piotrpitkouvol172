﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

	[System.Serializable]
	public class Pool {
		public string name;
		public GameObject prefab;
		public int size;
    }
	public static ObjectPooler Instance;
    public int activeTowers;
    int activeProjectiles;

	void Awake() {
		Instance = this;
    }

	public List<Pool> pools;
	public Dictionary<string, Queue<GameObject>> poolDictionary;

	void Start () {
        CreatePools();
        SpawnFromPool("Tower", transform.position, Quaternion.identity);
    }

    private void CreatePools() {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (Pool pool in pools) {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++) {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.name, objectPool);
        }
    }

    public void SpawnFromPool(string tag, Vector3 position, Quaternion rotation) {
		GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        if (tag=="Tower" && activeTowers > 99) return;
        while (objectToSpawn.activeSelf) {
            poolDictionary[tag].Enqueue(objectToSpawn);
            objectToSpawn = poolDictionary[tag].Dequeue();
        }
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;
        objectToSpawn.SetActive(true);
        if (tag == "Tower") {
            activeTowers += 1;
        }

        IPoolObj poolObj = objectToSpawn.GetComponent<IPoolObj>();

        if(poolObj != null) {
            poolObj.OnObjectSpawn();
        }

		poolDictionary[tag].Enqueue(objectToSpawn);
    }
}
