﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerCounter : MonoBehaviour {

	Text counterText;
	ObjectPooler objPooler;

	void Start() {
		counterText = GetComponent<Text>();
		objPooler = ObjectPooler.Instance;
    }

	void Update() {
		counterText.text = objPooler.activeTowers.ToString();
    }
}
