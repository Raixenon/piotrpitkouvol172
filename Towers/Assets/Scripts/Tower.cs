﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour, IPoolObj {

	[SerializeField] float timeBeforeFirstShooting = 6.0f;
	[SerializeField] float shootingFrequency = 0.5f;
    [SerializeField] int projectiles = 12;
    [SerializeField] GameObject gun;
    SpriteRenderer sr;
	float nextShooting = 0.0f;
    ObjectPooler projectilePooler;

	void Start () {
        OnObjectSpawn();
    }

    public void OnObjectSpawn() {
        projectilePooler = ObjectPooler.Instance;
        sr = GetComponent<SpriteRenderer>();
        sr.color = new Color(255, 0, 0);
        timeBeforeFirstShooting = 6.0f;
        projectiles = 12;
    }
	
	void FixedUpdate () {
        if (timeBeforeFirstShooting > Mathf.Epsilon) {
            timeBeforeFirstShooting -= Time.deltaTime;
            return;
        }
        nextShooting -= Time.deltaTime;
        TowerBehavior();
    }

    private void TowerBehavior() {
        if (nextShooting <= Mathf.Epsilon && projectiles > 0) {
            TowerRotation();
            TowerShooting();
        }
    }

    private void TowerRotation() {
        float randomRotation = Random.Range(15, 45);
        float zRotation = transform.rotation.eulerAngles.z + randomRotation;
        transform.rotation = Quaternion.Euler(0, 0, zRotation);
        nextShooting = shootingFrequency;
    }

    private void TowerShooting() {
        projectilePooler.SpawnFromPool("Projectile", gun.transform.position, gun.transform.rotation);
        projectiles -= 1;
        ChangeColor();
    }

    void ChangeColor() {
        if (projectiles == 0) {
            sr.color = new Color(255, 255, 255);
        }
    }
}
